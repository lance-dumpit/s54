let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue
 	print = () => collection;


// 2. Adds element to the rear of the queue
 enqueue = (element) => {
    if (collection.length === 0){
        collection[0] = element;
    }
    else{
        collection[collection.length] = element;
    }
    return collection;
};




// 3. Removes element from the front of the queue
 dequeue = () => {
    let newArray = [];
    for(let i = 0; i < collection.length - 1; i++){
        if(i===0){
            for(let j=i; j<collection.length-1; j++){
                collection[j] = collection[j+1];
            }
        }
        for(let i = 0; i < collection.length - 1; i++){
            newArray[i] = collection[i];
        }
    }
    collection.length = 0;
    for(i = 0; i < newArray.length; i++){
        collection[i] = newArray[i];
    }
    return collection;
};



// 4. Show element at the front
 front = () => collection[0];


// 5. Show the total number of elements
 size = () => collection.length;


// 6. Outputs a Boolean value describing whether queue is empty or not
 isEmpty = (collection) => collection === [];


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
